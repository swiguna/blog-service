package org.acme.hibernate.orm;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "posts")
@NamedQuery(name = "Posts.findAll", query = "SELECT f FROM Post f ORDER BY f.title", hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Cacheable
public class Post extends PanacheEntity {

    // @Id
    // @SequenceGenerator(name = "postsSequence", sequenceName = "posts_id_seq", allocationSize = 1, initialValue = 10)
    // @GeneratedValue(generator = "postsSequence")
    // private Integer id;

    // @Column(length = 40, unique = false)
    // private String title;

    // @Column(length = 200, unique = false)
    // private String content;
    
    // public void post() {
    // }

    // public void post(String title) {
    //     this.title = title;
    // }

    @Column(length = 40, unique = false)
    public String title;
    public String content;

    // @ManyToMany(cascade = CascadeType.ALL) will propagate beyond one level, delete will be cascaded
    @ManyToMany
    @JoinTable(name = "post_tag",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    public Set<Tag> tags = new HashSet<>();

    // public Integer getId() {
    //     return id;
    // }

    // public void setId(Integer id) {
    //     this.id = id;
    // }

    // public String getTitle() {
    //     return title;
    // }

    // public void setTitle(String title) {
    //     this.title = title;
    // }
    // public String getContent() {
    //     return content;
    // }

    // public void setContent(String content) {
    //     this.content = content;
    // }

}
