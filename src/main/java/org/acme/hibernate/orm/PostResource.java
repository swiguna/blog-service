package org.acme.hibernate.orm;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {

    @GET
    public List<Post> getAllPosts() {
        return Post.listAll();
    }

    @POST
    @Transactional
    public void createPost(Post post) {
        post.persist();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public void updatePost(@PathParam("id") Long id, Post updatedPost) {
        Post post = Post.findById(id);
        if (post != null) {
            post.title = updatedPost.title;
            post.content = updatedPost.content;
            post.persist();
        } else {
            throw new NotFoundException("Post not found with id: " + id);
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void deletePost(@PathParam("id") Long id) {
        Post post = Post.findById(id);
        if (post != null) {
            post.delete();
        } else {
            throw new NotFoundException("Post not found with id: " + id);
        }
    }
}