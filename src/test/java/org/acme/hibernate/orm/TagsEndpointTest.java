package org.acme.hibernate.orm;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsNot.not;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class TagsEndpointTest {

    @Test
    public void testListAllTags() {
        //List all, should have all 3 fruits the database has initially:
        given()
                .when().get("/api/tags")
                .then()
                .statusCode(200)
                .body(
                        containsString("Bali"),
                        containsString("Liburan"),
                        containsString("TagLabel3"));

        //Delete the Cherry:
        given()
                .when().delete("/api/tags/1")
                .then()
                .statusCode(204);

        //List all, Title1 should be missing now:
        given()
                .when().get("/api/tags")
                .then()
                .statusCode(200)
                .body(
                        not(containsString("Bali")),
                        containsString("Liburan"),
                        containsString("TagLabel3"));

        //Create the Pear:
        given()
                .when()
                .body("{\"label\" : \"Holiday\" }")
                .contentType("application/json")
                .post("/api/tags")
                .then()
                .statusCode(204);

        //List all, cherry should be missing now:
        given()
                .when().get("/api/tags")
                .then()
                .statusCode(200)
                .body(
                        not(containsString("Bali")),
                        containsString("Liburan"),
                        containsString("TagLabel3"),
                        containsString("Holiday"));
    }

}
