package org.acme.hibernate.orm;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/tags")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagResource {

    @GET
    public List<Tag> getAllTags() {
        return Tag.listAll();
    }

    @POST
    @Transactional
    public void createTag(Tag tag) {
        tag.persist();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public void updateTag(@PathParam("id") Long id, Tag updatedTag) {
        Tag tag = Tag.findById(id);
        if (tag != null) {
            tag.label = updatedTag.label;
            tag.persist();
        } else {
            throw new NotFoundException("Tag not found with id: " + id);
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void deleteTag(@PathParam("id") Long id) {
        Tag tag = Tag.findById(id);
        if (tag != null) {
            tag.delete();
        } else {
            throw new NotFoundException("Tag not found with id: " + id);
        }
    }
}