package org.acme.hibernate.orm;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsNot.not;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class PostsEndpointTest {

    @Test
    public void testListAllPosts() {
        //List all, should have all 3 fruits the database has initially:
        given()
                .when().get("/api/posts")
                .then()
                .statusCode(200)
                .body(
                        containsString("Title1"),
                        containsString("Title2"),
                        containsString("Title3"));

        //Delete the Cherry:
        given()
                .when().delete("/api/posts/1")
                .then()
                .statusCode(204);

        //List all, Title1 should be missing now:
        given()
                .when().get("/api/posts")
                .then()
                .statusCode(200)
                .body(
                        not(containsString("Title1")),
                        containsString("Title2"),
                        containsString("Title3"));

        //Create the Pear:
        given()
                .when()
                .body("{\"title\" : \"Title4\", \"content\" : \"content of Title4\" }")
                .contentType("application/json")
                .post("/api/posts")
                .then()
                .statusCode(204);

        //List all, cherry should be missing now:
        given()
                .when().get("/api/posts")
                .then()
                .statusCode(200)
                .body(
                        not(containsString("Title1")),
                        containsString("Title2"),
                        containsString("Title3"),
                        containsString("content"));
    }

}
