package org.acme.hibernate.orm;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.ManyToMany;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.QueryHint;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "tags")
@NamedQuery(name = "Tags.findAll", query = "SELECT f FROM Tag f ORDER BY f.label", hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Cacheable
public class Tag extends PanacheEntity {

    // @Id
    // @SequenceGenerator(name = "tagsSequence", sequenceName = "tags_id_seq", allocationSize = 1, initialValue = 10)
    // @GeneratedValue(generator = "tagsSequence")
    // private Integer id;

    // @Column(length = 40, unique = true)
    // private String label;

    // public void tag() {
    // }
    @Column(length = 40, unique = true)
    public String label;

    @ManyToMany(mappedBy = "tags")
    @JsonIgnore // Add this annotation to break the circular reference
    public Set<Post> posts = new HashSet<>();

    // public void tag(String label) {
    //     this.label = label;
    // }

    // public Integer getId() {
    //     return id;
    // }

    // public void setId(Integer id) {
    //     this.id = id;
    // }

    // public String getLabel() {
    //     return label;
    // }

    // public void setLabel(String label) {
    //     this.label = label;
    // }

}
